/**
 * Homepage script
 **/
(function($){
    $(document).ready(function() 
    {  
        $('.news-headline').flexslider({
            animation: "slide",
            directionNav: false
        });
        $('.top-flash').flexslider({
            animation: "slide",
            animationLoop: true,
            itemWidth: 210,
            itemMargin: 0,
            minItems: 1,
            maxItems: 3,
            controlNav: false,
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>'
        });
    });
})(jQuery);