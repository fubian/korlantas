/*
 *===========================================================
 * Fade Menu
 *===========================================================*/
(function($){
 
    $.fn.izn_fade_menu = function(){
        
        $(this).each( function(){
            var mouse_in_menu = false;
            var root = $(this);
            
            $( '.b_fade-menu_link', root ).click( function(){
            
                $( '.b_fade-menu_content' ).fadeOut( "fast" );
                $( '.b_fade-menu_link' ).removeClass( 'active' );
            
                var link    = $(this);
                var content = $( '.b_fade-menu_content', root );
                
                if ( content.is(":visible") ) {
                    content.fadeOut( "fast" );
                    link.removeClass( 'active' );
                    $( '.b_page_overlay' ).hide();
                } else {
                    content.fadeIn( "fast" );
                    link.addClass( 'active' );
                    $( '.b_page_overlay' ).show();
                }
                return false;
            });
            $( '.b_fade-menu_content', root ).hover( function(){
                    mouse_in_menu = true;
                }, function(){
                    mouse_in_menu = false;
            });
            $( "body" ).click(function(){
                if ( ! mouse_in_menu ) {
                    $( '.b_fade-menu_content', root ).fadeOut( "fast" );
                    $( '.b_fade-menu_link', root ).removeClass( 'active' );
                    $( '.b_page_overlay' ).hide();
                }
            });
            $( '.b_fade-menu_content a', root ).click(function(){
                $( '.b_fade-menu_content', root ).fadeOut( "fast" );
                $( '.b_fade-menu_link', root ).removeClass( 'active' );
                $( '.b_page_overlay' ).hide();
            });
        });
    };
})(jQuery);