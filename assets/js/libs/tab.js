/*
 *===========================================================
 * Tab menu
 *===========================================================*/
(function($)
{
    $.fn.izn_tab = function(options)
    {
        var defaults  = {
            after_click:     function(){},
            tab_init:        function(){}
        };
        var opt  = $.extend( defaults, options );
        
        this.each( function()
        {
            var root = $(this);
            
            tab_init();
            
            $( '.b_tab_menu a', root ).click( function()
            {
                clear_tab();
                $( this ).addClass( 'active' );
                
                var idtab = $(this).attr( 'href' ).replace( '#', '' );
                $( '#'+idtab ).show();
                
                opt.after_click( idtab, root );
                
                return false;
            });
            
            function clear_tab() 
            {
                $( '.b_tab_menu a', root ).each( function(){
                    $(this).removeClass( 'active' );
                });
                $( '.b_tab_content_item', root ).hide();
            };
            
            function tab_init() 
            {
                clear_tab();
                $( '.b_tab_content_item', root ).hide();
                var current = $( '.b_tab_menu a:first-child', root ).attr( 'href' ).replace( '#', '' );
                $( '#'+current ).show();
                $( 'a[href="#'+current+'"]' ).addClass( 'active' );
                
                opt.tab_init( current, root );
            };
        });
    };
    
})(jQuery);