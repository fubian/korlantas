module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            // 2. Configuration for concatinating files goes here.
            
            //General
            general: {
                src: [
                    'assets/js/libs/*.js',
                    'assets/js/page/general.js',
                ],
                dest: 'assets/js/general.js',
            },
            //Homepage
            homepage: {
                src: [
                    'assets/js/general.js',
                    'assets/js/page/homepage.js'
                ],
                dest: 'assets/js/homepage.js',
            },
            //Education
            education: {
                src: [
                    'assets/js/general.js',
                    'assets/js/page/education.js'
                ],
                dest: 'assets/js/education.js',
            },
        },
        
        uglify: {
        
            //Homepage
            home: {
                src: 'assets/js/homepage.js',
                dest: 'assets/js/homepage.min.js'
            },
        
            //Education
            education: {
                src: 'assets/js/education.js',
                dest: 'assets/js/education.min.js'
            },
        },
        less: {
        
            /*
             * Compress
             */
            compress: {
                options: {
                    paths: [
                        "assets/css/less"
                    ],
                    compress: true
                },
                files: {
                    //Homepage
                    "assets/css/homepage.min.css": "assets/css/less/page/homepage.less",
                    //Education
                    "assets/css/education.min.css": "assets/css/less/page/education.less",
                }
            },
        
            /*
             * Uncompress
             */
            uncompress: {
                options: {
                    paths: [
                        "assets/css/less"
                    ],
                    compress: false
                },
                files: {
                    //Homepage
                    "assets/css/homepage.css": "assets/css/less/page/homepage.less",
                    //Education
                    "assets/css/education.css": "assets/css/less/page/education.less",
                }
            },
        },
        watch: {
            js_concat: {
                files: ['assets/**/*.js'],
                tasks: ['concat'],
                options: {
                    spawn: false,
                },
            },
            js_uglify: {
                files: ['assets/**/*.js'],
                tasks: ['uglify'],
                options: {
                    spawn: false,
                },
            },
            css_uglify: {
                files: ['assets/**/*.css'],
                tasks: ['uglify'],
                options: {
                    spawn: false,
                },
            },
            doless: {
                files: ['assets/**/*.less'],
                tasks: ['less'],
                options: {
                    spawn: false,
                },
            }
        }
    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['concat','uglify','watch','less']);

};