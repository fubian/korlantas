<!DOCTYPE html>
<html lang="en">
<head>
    <title>Website Resmi Korlantas</title>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="Website Resmi Korlantas">
    <meta name="keywords" content="korlantas, police, law">
    
    <link href="assets/css/homepage.min.css" rel="stylesheet">
    
</head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- start: .root -->
<div class="root">
    <!-- start: .wrapper -->
    <div class="wrapper">
        <!-- start: .inner -->
        <div class="inner">
            
            <!-- start: .top -->
            <header class="top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="top_site-logo">
                                <img src="assets/img/logo-korlantas.png" alt="Korlantas Polri">
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="top_site-title">
                                <h1>Korlantas Polri</h1>
                                <h2>Korps Lalu Lintas Polri</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top_site-menu">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <nav class="top_site-menu_nav">
                                    <ul class="clear">
                                        <li>
                                            <a href="">Home</a>
                                        </li>
                                        <li>
                                            <a href="">About Us</a>
                                        </li>
                                        <li>
                                            <a href="">CCTV</a>
                                        </li>
                                        <li>
                                            <a href="">Info</a>
                                        </li>
                                        <li>
                                            <a href="">Edukasi</a>
                                        </li>
                                        <li>
                                            <a href="">Contact Us</a>
                                        </li>
                                        <li>
                                            <a href="">Layanan Masyarakat</a>
                                        </li>
                                        <li>
                                            <a href=""><i class="fa fa-user"></i></a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- end: .top -->
            
            <section id="wraper-carousel" class="block-video">
                <div class="container">
                    <div class="top-flash flexslider">
                        <ul class="slides clear">
                            <li class="block-video_item clear">
                                <div class="block-video_item_inner">
                                    <div class="block-video_item_thumb">
                                        <img src="assets/img/video.jpg" />
                                    </div>
                                    <div class="block-video_item_title">
                                        <a href="">Kemacetan dimana-mana Jakarta Lumpuh Total</a>
                                        <span class="post-date">Posted On October 4, 2014</span>
                                    </div>
                                </div>
                            </li>
                            <li class="block-video_item clear">
                                <div class="block-video_item_inner">
                                    <div class="block-video_item_thumb">
                                        <img src="assets/img/video.jpg" />
                                    </div>
                                    <div class="block-video_item_title">
                                        <a href="">Kemacetan dimana-mana Jakarta Lumpuh Total</a>
                                        <span class="post-date">Posted On October 4, 2014</span>
                                    </div>
                                </div>
                            </li>
                            <li class="block-video_item clear">
                                <div class="block-video_item_inner">
                                    <div class="block-video_item_thumb">
                                        <img src="assets/img/video.jpg" />
                                    </div>
                                    <div class="block-video_item_title">
                                        <a href="">Kemacetan dimana-mana Jakarta Lumpuh Total</a>
                                        <span class="post-date">Posted On October 4, 2014</span>
                                    </div>
                                </div>
                            </li>
                            <li class="block-video_item clear">
                                <div class="block-video_item_inner">
                                    <div class="block-video_item_thumb">
                                        <img src="assets/img/video.jpg" />
                                    </div>
                                    <div class="block-video_item_title">
                                        <a href="">Kemacetan dimana-mana Jakarta Lumpuh Total</a>
                                        <span class="post-date">Posted On October 4, 2014</span>
                                    </div>
                                </div>
                            </li>
                            <!-- items mirrored twice, total of 12 -->
                          </ul>
                    </div>
                </div>
            </section>
            
            <!-- start: main -->
            <main>
                <div class="content_main">
                    <div class="container">
                        <div class="row">
                            <!-- start: .primary -->
                            <div class="primary col-md-8">
                            
                                <!-- start: .news-headline -->
                                <div class="news-headline flexslider">
                                
                                    <ul class="slides">
                                    
                                        <!-- start: .news-headline_item -->
                                        <li class="news-headline_item">
                                            <div class="news-headline_item_thumb">
                                                <img src="assets/img/headline-1.jpg" alt="">
                                            </div>
                                            <div class="news-headline_item_content">
                                                <div class="news-headline_item_category">News</div>
                                                <h2 class="news-headline_item_title">
                                                    <a href="">Kemacetan Panjang, Razia Operasi Zebra Berhasil Mengamankan Ratusan Kendaraan Bodong</a>
                                                </h2>
                                                <div class="news-headline_item_link">
                                                    <a href="">Read more <i class="fa fa-long-arrow-right"></i></a>
                                                </div>
                                            </div>
                                            <div class="news-headline_item_meta">
                                                <span class="news-headline_item_meta_info">
                                                    <i class="fa fa-comments"></i>
                                                    45
                                                </span>
                                                <span class="news-headline_item_meta_info">
                                                    <i class="fa fa-heart"></i>
                                                    23
                                                </span>
                                            </div>
                                        </li>
                                        <!-- end: .news-headline_item -->
                                    
                                        <!-- start: .news-headline_item -->
                                        <li class="news-headline_item">
                                            <div class="news-headline_item_thumb">
                                                <img src="assets/img/headline-1.jpg" alt="">
                                            </div>
                                            <div class="news-headline_item_content">
                                                <div class="news-headline_item_category">News</div>
                                                <h2 class="news-headline_item_title">
                                                    <a href="">Kemacetan Panjang, Razia Operasi Zebra Berhasil Mengamankan Ratusan Kendaraan Bodong</a>
                                                </h2>
                                                <div class="news-headline_item_link">
                                                    <a href="">Read more <i class="fa fa-long-arrow-right"></i></a>
                                                </div>
                                            </div>
                                            <div class="news-headline_item_meta">
                                                <span class="news-headline_item_meta_info">
                                                    <i class="fa fa-comments"></i>
                                                    45
                                                </span>
                                                <span class="news-headline_item_meta_info">
                                                    <i class="fa fa-heart"></i>
                                                    23
                                                </span>
                                            </div>
                                        </li>
                                        <!-- end: .news-headline_item -->
                                    
                                        <!-- start: .news-headline_item -->
                                        <li class="news-headline_item">
                                            <div class="news-headline_item_thumb">
                                                <img src="assets/img/headline-1.jpg" alt="">
                                            </div>
                                            <div class="news-headline_item_content">
                                                <div class="news-headline_item_category">News</div>
                                                <h2 class="news-headline_item_title">
                                                    <a href="">Kemacetan Panjang, Razia Operasi Zebra Berhasil Mengamankan Ratusan Kendaraan Bodong</a>
                                                </h2>
                                                <div class="news-headline_item_link">
                                                    <a href="">Read more <i class="fa fa-long-arrow-right"></i></a>
                                                </div>
                                            </div>
                                            <div class="news-headline_item_meta">
                                                <span class="news-headline_item_meta_info">
                                                    <i class="fa fa-comments"></i>
                                                    45
                                                </span>
                                                <span class="news-headline_item_meta_info">
                                                    <i class="fa fa-heart"></i>
                                                    23
                                                </span>
                                            </div>
                                        </li>
                                        <!-- end: .news-headline_item -->
                                        
                                    </ul>
                                </div>
                                <!-- end: .news-headline -->
                            
                                <!-- start: .news-main -->
                                <div class="news-main">
                                
                                    <!-- start: .news-main_item -->
                                    <div class="news-main_item">
                                        <div class="news-main_item_thumb">
                                            <img src="assets/img/category-1.jpg" alt="">
                                        </div>
                                        <!-- start: .news-main_item_main -->
                                        <div class="news-main_item_main">
                                            <div class="news-main_item_content">
                                            
                                                <!-- start: .news-main_item_meta -->
                                                <div class="news-main_item_meta">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <strong class="news-main_item_author">By Dian Kurnia</strong>
                                                            <span class="dot">.</span>
                                                            <strong class="news-main_item_date">Oct 8, 2014</strong>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <span class="news-main_item_comment">
                                                                <strong>34</strong>
                                                                <i class="fa fa-comments"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end: .news-main_item_meta -->
                                                
                                                <!-- start: .news-main_item_title -->
                                                <h2 class="news-main_item_title">
                                                    <a href="">Jadwal Pelayanan SIM Keliling Ditambah, Masyarakat Semakin Leluasa Memperpanjang SIM</a>
                                                </h2>
                                                <!-- end: .news-main_item_title -->
                                                
                                                <!-- start: .news-main_item_text -->
                                                <div class="news-main_item_text">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget finibus felis, id auctor enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque tincidunt orci tortor, ut suscipit nisi condimentum vel.</p>
                                                    <ul>
                                                        <li>Pellentesque sed odio mattis, molestie velit nec, varius risus.</li>
                                                        <li>Donec vulputate tellus nec lectus sollicitudin, nec cursus lacus consectetur.</li>
                                                        <li>Fusce non leo facilisis, pulvinar orci sit amet, mollis velit.</li>
                                                    </ul>
                                                </div>
                                                
                                            </div>
                                            <div class="news-main_item_share">
                                                <h3>Shares</h3>
                                                <ul>
                                                    <li class="news-main_item_share_facebook">
                                                        <a href=""><i class="fa fa-facebook"></i></a>
                                                    </li>
                                                    <li class="news-main_item_share_twitter">
                                                        <a href=""><i class="fa fa-twitter"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end: .news-main_item_main -->
                                        
                                        <div class="news-main_item_ribbon">
                                            <strong>Info Pelayanan</strong>
                                        </div>
                                    </div>
                                    <!-- end: .news-main_item -->
                                
                                    <!-- start: .news-main_item -->
                                    <div class="news-main_item">
                                        <div class="news-main_item_thumb">
                                            <img src="assets/img/category-2.jpg" alt="">
                                        </div>
                                        <!-- start: .news-main_item_main -->
                                        <div class="news-main_item_main">
                                            <div class="news-main_item_content">
                                            
                                                <!-- start: .news-main_item_meta -->
                                                <div class="news-main_item_meta">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <strong class="news-main_item_author">By Dian Kurnia</strong>
                                                            <span class="dot">.</span>
                                                            <strong class="news-main_item_date">Oct 8, 2014</strong>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <span class="news-main_item_comment">
                                                                <strong>34</strong>
                                                                <i class="fa fa-comments"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end: .news-main_item_meta -->
                                                
                                                <!-- start: .news-main_item_title -->
                                                <h2 class="news-main_item_title">
                                                    <a href="">Jadwal Pelayanan SIM Keliling Ditambah, Masyarakat Semakin Leluasa Memperpanjang SIM</a>
                                                </h2>
                                                <!-- end: .news-main_item_title -->
                                                
                                                <!-- start: .news-main_item_text -->
                                                <div class="news-main_item_text">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget finibus felis, id auctor enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque tincidunt orci tortor, ut suscipit nisi condimentum vel.</p>
                                                    <ul>
                                                        <li>Pellentesque sed odio mattis, molestie velit nec, varius risus.</li>
                                                        <li>Donec vulputate tellus nec lectus sollicitudin, nec cursus lacus consectetur.</li>
                                                        <li>Fusce non leo facilisis, pulvinar orci sit amet, mollis velit.</li>
                                                    </ul>
                                                </div>
                                                
                                            </div>
                                            <div class="news-main_item_share">
                                                <h3>Shares</h3>
                                                <ul>
                                                    <li class="news-main_item_share_facebook">
                                                        <a href=""><i class="fa fa-facebook"></i></a>
                                                    </li>
                                                    <li class="news-main_item_share_twitter">
                                                        <a href=""><i class="fa fa-twitter"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end: .news-main_item_main -->
                                        
                                        <div class="news-main_item_ribbon">
                                            <strong>Edukasi</strong>
                                        </div>
                                    </div>
                                    <!-- end: .news-main_item -->
                                
                                    <!-- start: .news-main_item -->
                                    <div class="news-main_item">
                                        <div class="news-main_item_thumb">
                                            <img src="assets/img/category-3.jpg" alt="">
                                        </div>
                                        <!-- start: .news-main_item_main -->
                                        <div class="news-main_item_main">
                                            <div class="news-main_item_content">
                                            
                                                <!-- start: .news-main_item_meta -->
                                                <div class="news-main_item_meta">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <strong class="news-main_item_author">By Dian Kurnia</strong>
                                                            <span class="dot">.</span>
                                                            <strong class="news-main_item_date">Oct 8, 2014</strong>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <span class="news-main_item_comment">
                                                                <strong>34</strong>
                                                                <i class="fa fa-comments"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end: .news-main_item_meta -->
                                                
                                                <!-- start: .news-main_item_title -->
                                                <h2 class="news-main_item_title">
                                                    <a href="">Jadwal Pelayanan SIM Keliling Ditambah, Masyarakat Semakin Leluasa Memperpanjang SIM</a>
                                                </h2>
                                                <!-- end: .news-main_item_title -->
                                                
                                                <!-- start: .news-main_item_text -->
                                                <div class="news-main_item_text">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget finibus felis, id auctor enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque tincidunt orci tortor, ut suscipit nisi condimentum vel.</p>
                                                    <ul>
                                                        <li>Pellentesque sed odio mattis, molestie velit nec, varius risus.</li>
                                                        <li>Donec vulputate tellus nec lectus sollicitudin, nec cursus lacus consectetur.</li>
                                                        <li>Fusce non leo facilisis, pulvinar orci sit amet, mollis velit.</li>
                                                    </ul>
                                                </div>
                                                
                                            </div>
                                            <div class="news-main_item_share">
                                                <h3>Shares</h3>
                                                <ul>
                                                    <li class="news-main_item_share_facebook">
                                                        <a href=""><i class="fa fa-facebook"></i></a>
                                                    </li>
                                                    <li class="news-main_item_share_twitter">
                                                        <a href=""><i class="fa fa-twitter"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end: .news-main_item_main -->
                                        
                                        <div class="news-main_item_ribbon">
                                            <strong>Info Kecelakaan Lantas</strong>
                                        </div>
                                    </div>
                                    <!-- end: .news-main_item -->
                                    
                                </div>
                                <!-- start: .news-main -->
                            
                                <!-- start: .cctv-video -->
                                <div class="cctv-video">
                                    <img src="assets/img/cctv-video-img.jpg" alt="">
                                </div>
                                <!-- end: .cctv-video -->
                                
                            </div>
                            <!-- end: .primary -->
                            
                            <!-- start: .sidebar -->
                            <div class="sidebar col-md-4">
                                <aside>
                                    <ul>
                                        <!-- start: .widget_today -->
                                        <li class="widget widget_today">
                                            <div class="widget_inner">
                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <p class="widget_today_date">Today is: Monday, December 17, 2014</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="widget_today_celcius">
                                                            <i class="fa fa-cloud"></i>
                                                            <strong>63 <sup>o</sup></strong>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- end: .widget_today -->
                                        
                                        <!-- start: .widget_stat -->
                                        <li class="widget widget_stat">
                                            <div class="widget_inner">
                                                <div class="clear">
                                                    <div class="col-md-4 widget_stat_item widget_stat_item_facebook">
                                                        <i class="fa fa-facebook"></i>
                                                        <strong class="widget_stat_item_value">3256</strong>
                                                        <em class="widget_stat_item_label">Likes</em>
                                                    </div>
                                                    <div class="col-md-4 widget_stat_item widget_stat_item_twitter">
                                                        <i class="fa fa-twitter"></i>
                                                        <strong class="widget_stat_item_value">12K</strong>
                                                        <em class="widget_stat_item_label">Followers</em>
                                                    </div>
                                                    <div class="col-md-4 widget_stat_item widget_stat_item_visitor">
                                                        <i class="fa fa-user"></i>
                                                        <strong class="widget_stat_item_value">12M</strong>
                                                        <em class="widget_stat_item_label">VIsitors</em>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- end: .widget_stat -->
                                        
                                        <!-- start: .widget_search -->
                                        <li class="widget widget_search widget_shadow">
                                            <div class="widget_inner">
                                                <h3>Search For More News</h3>
                                                <div class="widget_search_input">
                                                    <form action="" method="get">
                                                        <input type="text" name="keyword" value="" placeholder="Search">
                                                        <i class="fa fa-search"></i>
                                                    </form>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- end: .widget_search -->
                                        
                                        <!-- start: .widget_hot-news -->
                                        <li class="widget widget_hot-news widget_shadow">
                                            <div class="widget_inner">
                                                <div class="widget_title">
                                                    <h3><span>Hot News</span></h3>
                                                </div>
                                                <div class="widget_hot-news_list">
                                                    <div class="widget_hot-news_item clear">
                                                        <div class="widget_hot-news_item_thumb">
                                                            <img src="assets/img/news-1.jpg" alt="">
                                                        </div>
                                                        <div class="widget_hot-news_item_content">
                                                            <div class="widget_hot-news_item_category">
                                                                Sosialita
                                                            </div>
                                                            <h3 class="widget_hot-news_item_title">Bentrok di Timika, 100 Orang Tewas</h3>
                                                        </div>
                                                    </div>
                                                    <div class="widget_hot-news_item clear">
                                                        <div class="widget_hot-news_item_thumb">
                                                            <img src="assets/img/news-2.jpg" alt="">
                                                        </div>
                                                        <div class="widget_hot-news_item_content">
                                                            <div class="widget_hot-news_item_category">
                                                                Hiburan
                                                            </div>
                                                            <h3 class="widget_hot-news_item_title">Kuartet Pembawa Berita Lalu Lintas Beraksi</h3>
                                                        </div>
                                                    </div>
                                                    <div class="widget_hot-news_item clear">
                                                        <div class="widget_hot-news_item_thumb">
                                                            <img src="assets/img/news-3.jpg" alt="">
                                                        </div>
                                                        <div class="widget_hot-news_item_content">
                                                            <div class="widget_hot-news_item_category">
                                                                Teknologi
                                                            </div>
                                                            <h3 class="widget_hot-news_item_title">Polri Dilengkapi Peralatan dan Gear Baru</h3>
                                                        </div>
                                                    </div>
                                                    <div class="widget_hot-news_item clear">
                                                        <div class="widget_hot-news_item_thumb">
                                                            <img src="assets/img/news-4.jpg" alt="">
                                                        </div>
                                                        <div class="widget_hot-news_item_content">
                                                            <div class="widget_hot-news_item_category">
                                                                Edukasi
                                                            </div>
                                                            <h3 class="widget_hot-news_item_title">Safety Riding SMAN 3 Untuk Keselamatan Berkendara</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- end: .widget_hot-news -->
                                        
                                        <!-- start: .widget_ad -->
                                        <li class="widget widget_adds widget_shadow">
                                            <div class="widget_inner">
                                               <img src="assets/img/sb-ad-1.jpg" alt="">
                                            </div>
                                        </li>
                                        <!-- end: .widget_ad -->
                                         
                                        <!-- start: .widget_tweet -->
                                        <li class="widget widget_tweet widget_shadow">
                                            <div class="widget_inner">
                                                <div class="widget_title">
                                                    <h3><span>Twitter Feed</span></h3>
                                                </div>
                                                <div class="widget_tweet_list">
                                                    <div class="widget_tweet_item clear">
                                                        <div class="widget_tweet_item_thumb">
                                                            <img src="assets/img/logo-korlantas-sm.jpg" alt="">
                                                        </div>
                                                        <div class="widget_tweet_item_text">
                                                            <h4 class="widget_tweet_item_name">
                                                                <a href="">Bambang Sudibyo</a>
                                                            </h4>
                                                            <div class="widget_tweet_item_content">
                                                                Telah terjadi kecelakaan di ruas jalan manggarai, 2 tewas <a href="">http://bit.ly/1ujk78</a>
                                                            </div>
                                                            <div class="widget_tweet_item_time">
                                                                2 hours ago
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="widget_tweet_item clear">
                                                        <div class="widget_tweet_item_thumb">
                                                            <img src="assets/img/logo-korlantas-sm.jpg" alt="">
                                                        </div>
                                                        <div class="widget_tweet_item_text">
                                                            <h4 class="widget_tweet_item_name">
                                                                <a href="">Bambang Sudibyo</a>
                                                            </h4>
                                                            <div class="widget_tweet_item_content">
                                                                Telah terjadi kecelakaan di ruas jalan manggarai, 2 tewas <a href="">http://bit.ly/1ujk78</a>
                                                            </div>
                                                            <div class="widget_tweet_item_time">
                                                                2 hours ago
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="widget_tweet_item clear">
                                                        <div class="widget_tweet_item_thumb">
                                                            <img src="assets/img/logo-korlantas-sm.jpg" alt="">
                                                        </div>
                                                        <div class="widget_tweet_item_text">
                                                            <h4 class="widget_tweet_item_name">
                                                                <a href="">Bambang Sudibyo</a>
                                                            </h4>
                                                            <div class="widget_tweet_item_content">
                                                                Telah terjadi kecelakaan di ruas jalan manggarai, 2 tewas <a href="">http://bit.ly/1ujk78</a>
                                                            </div>
                                                            <div class="widget_tweet_item_time">
                                                                2 hours ago
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- end: .widget_tweet -->
                                        
                                        <!-- start: .widget_ym -->
                                        <li class="widget widget_ym widget_shadow">
                                            <div class="widget_inner">
                                                <a href="">
                                                    <img src="assets/img/ym-online.png" alt="Online">
                                                </a>
                                            </div>
                                        </li>
                                        <!-- end: .widget_ym -->
                                        
                                        <!-- start: .widget_facebook -->
                                        <li class="widget widget_facebook widget_shadow">
                                            <div class="widget_inner">
                                                <div class="widget_title">
                                                    <h3><span>Like Us On Facebook</span></h3>
                                                </div>
                                                <div class="fb-like-box" data-href="https://www.facebook.com/NTMCPOLRI" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
                                            </div>
                                        </li>
                                        <!-- end: .widget_facebook -->
                                        
                                        <!-- start: .widget_video -->
                                        <li class="widget widget_video widget_shadow">
                                            <div class="widget_inner">
                                                <div class="widget_title">
                                                    <h3>Video</h3>
                                                </div>
                                                <div class="widget_video_list">
                                                    <div class="widget_video_item clear">
                                                        <div class="widget_video_item_thumb">
                                                            <img src="assets/img/video.jpg" alt="">
                                                        </div>
                                                        <div class="widget_video_item_info">
                                                            <h3><a href="">Kemacetan Jalan Tamrin Hingga Mencapai 70KM</a></h3>
                                                            <div class="widget_video_item_meta">
                                                                <span class="widget_video_item_meta_info">
                                                                    <i class="fa fa-eye"></i>
                                                                    <strong>23 views</strong>
                                                                </span>
                                                                <span class="widget_video_item_meta_info">
                                                                    <i class="fa fa-heart"></i>
                                                                    <strong>48 likes</strong>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="widget_video_item clear">
                                                        <div class="widget_video_item_thumb">
                                                            <img src="assets/img/video.jpg" alt="">
                                                        </div>
                                                        <div class="widget_video_item_info">
                                                            <h3><a href="">Kemacetan Jalan Tamrin Hingga Mencapai 70KM</a></h3>
                                                            <div class="widget_video_item_meta">
                                                                <span class="widget_video_item_meta_info">
                                                                    <i class="fa fa-eye"></i>
                                                                    <strong>23 views</strong>
                                                                </span>
                                                                <span class="widget_video_item_meta_info">
                                                                    <i class="fa fa-heart"></i>
                                                                    <strong>48 likes</strong>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="widget_video_item clear">
                                                        <div class="widget_video_item_thumb">
                                                            <img src="assets/img/video.jpg" alt="">
                                                        </div>
                                                        <div class="widget_video_item_info">
                                                            <h3><a href="">Kemacetan Jalan Tamrin Hingga Mencapai 70KM</a></h3>
                                                            <div class="widget_video_item_meta">
                                                                <span class="widget_video_item_meta_info">
                                                                    <i class="fa fa-eye"></i>
                                                                    <strong>23 views</strong>
                                                                </span>
                                                                <span class="widget_video_item_meta_info">
                                                                    <i class="fa fa-heart"></i>
                                                                    <strong>48 likes</strong>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="widget_video_more">
                                                    <a href="">See more our videos <i class="fa fa-caret-right"></i></a>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- end: .widget_tweet -->
                                    </ul>
                                </aside>
                            </div>
                            <!-- end: .sidebar -->
                            
                        </div>
                    </div>
                </div>
                <div class="content">
                    <!-- start: .content_category -->
                    <div class="content_category">
                        <div class="container">
                            <div class="row">
                                <!-- start: .content_category_item -->
                                <div class="content_category_item col-md-3 col-sm-6">
                                    <div class="content_category_item_title">
                                        <h2>Info Pelayanan</h2>
                                        <i class="fa fa-long-arrow-right"></i>
                                    </div>
                                    <div class="content_category_item_main">
                                        <img src="assets/img/other-1.jpg" alt="">
                                        <h3>Tata Cara dan Persyaratan Untuk Pengajuan SIM</h3>
                                    </div>
                                    <div class="content_category_item_other">
                                        <ul>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Tata Cara dan Persyaratan Untuk Pengajuan SIM</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Tata Cara dan Persyaratan Untuk Pengajuan SIM</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Tata Cara dan Persyaratan Untuk Pengajuan SIM</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Tata Cara dan Persyaratan Untuk Pengajuan SIM</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- end: .content_category_item -->
                                
                                <!-- start: .content_category_item -->
                                <div class="content_category_item col-md-3 col-sm-6">
                                    <div class="content_category_item_title">
                                        <h2>Info Kecelakaan Lalu Lintas</h2>
                                        <i class="fa fa-long-arrow-right"></i>
                                    </div>
                                    <div class="content_category_item_main">
                                        <img src="assets/img/other-2.jpg" alt="">
                                        <h3>Waspadai 5 Daerah Dengan Tingkat Kecelakaan Tinggi</h3>
                                    </div>
                                    <div class="content_category_item_other">
                                        <ul>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Waspadai 5 Daerah Dengan Tingkat Kecelakaan Tinggi</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Waspadai 5 Daerah Dengan Tingkat Kecelakaan Tinggi</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Waspadai 5 Daerah Dengan Tingkat Kecelakaan Tinggi</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Waspadai 5 Daerah Dengan Tingkat Kecelakaan Tinggi</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- end: .content_category_item -->
                                
                                <!-- start: .content_category_item -->
                                <div class="content_category_item col-md-3 col-sm-6">
                                    <div class="content_category_item_title">
                                        <h2>Info Pelanggaran Lalu Lintas</h2>
                                        <i class="fa fa-long-arrow-right"></i>
                                    </div>
                                    <div class="content_category_item_main">
                                        <img src="assets/img/other-3.jpg" alt="">
                                        <h3>Tertib Berlalu Lintas Sesuai Jalur Kendaraan</h3>
                                    </div>
                                    <div class="content_category_item_other">
                                        <ul>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Tertib Berlalu Lintas Sesuai Jalur Kendaraan</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Tertib Berlalu Lintas Sesuai Jalur Kendaraan</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Tertib Berlalu Lintas Sesuai Jalur Kendaraan</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Tertib Berlalu Lintas Sesuai Jalur Kendaraan</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- end: .content_category_item -->
                                
                                <!-- start: .content_category_item -->
                                <div class="content_category_item col-md-3 col-sm-6">
                                    <div class="content_category_item_title">
                                        <h2>Info Hilang Temu</h2>
                                        <i class="fa fa-long-arrow-right"></i>
                                    </div>
                                    <div class="content_category_item_main">
                                        <img src="assets/img/other-4.jpg" alt="">
                                        <h3>Konfirmasi Kehilangan dan Penemuan Orang/Barang</h3>
                                    </div>
                                    <div class="content_category_item_other">
                                        <ul>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Konfirmasi Kehilangan dan Penemuan Orang/Barang</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Konfirmasi Kehilangan dan Penemuan Orang/Barang</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Konfirmasi Kehilangan dan Penemuan Orang/Barang</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-file-text-o"></i>
                                                <a href="">Konfirmasi Kehilangan dan Penemuan Orang/Barang</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- end: .content_category_item -->
                            </div>
                        </div>
                    </div>
                    <!-- end: .content_category -->
                </div>
            </main>
            <!-- end: main -->
            
            <!-- start: .bottom -->
            <div class="bottom">
                <div class="bottom_content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 section">
                                <div class="section_inner bottom_about-us">
                                    <h2 class="section_title">About Us</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                                </div>
                            </div>
                            <div class="col-md-4 section">
                                <div class="section_inner bottom_news">
                                    <h2 class="section_title">Latest News</h2>
                                    <div class="bottom_news_list">
                                        <div class="bottom_news_item clear">
                                            <div class="bottom_news_item_thumb">
                                                <img src="assets/img/news-1.jpg" alt="">
                                            </div>
                                            <div class="bottom_news_item_content">
                                                <div class="bottom_news_item_category">
                                                    Sosialita
                                                </div>
                                                <h3 class="bottom_news_item_title">Bentrok di Timika, 100 Orang Tewas</h3>
                                            </div>
                                        </div>
                                        <div class="bottom_news_item clear">
                                            <div class="bottom_news_item_thumb">
                                                <img src="assets/img/news-2.jpg" alt="">
                                            </div>
                                            <div class="bottom_news_item_content">
                                                <div class="bottom_news_item_category">
                                                    Hiburan
                                                </div>
                                                <h3 class="bottom_news_item_title">Kuartet Pembawa Berita Lalu Lintas Beraksi</h3>
                                            </div>
                                        </div>
                                        <div class="bottom_news_item clear">
                                            <div class="bottom_news_item_thumb">
                                                <img src="assets/img/news-3.jpg" alt="">
                                            </div>
                                            <div class="bottom_news_item_content">
                                                <div class="bottom_news_item_category">
                                                    Teknologi
                                                </div>
                                                <h3 class="bottom_news_item_title">Polri Dilengkapi Peralatan dan Gear Baru</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 section">
                                <div class="section_inner">
                                    <h2 class="section_title">News Line Navigation</h2>
                                    <nav class="bottom_nav">
                                        <ul>
                                            <li><i class="fa fa-caret-right"></i><a href="">Home</a></li>
                                            <li><i class="fa fa-caret-right"></i><a href="">About Us</a></li>
                                            <li><i class="fa fa-caret-right"></i><a href="">CCTV</a></li>
                                            <li><i class="fa fa-caret-right"></i><a href="">Info</a></li>
                                            <li><i class="fa fa-caret-right"></i><a href="">Edukasi</a></li>
                                            <li><i class="fa fa-caret-right"></i><a href="">Contact Us</a></li>
                                            <li><i class="fa fa-caret-right"></i><a href="">Layanan Masyarakat</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="footer_copyright"><small>Copyright &copy; NTMC Polri</small></div>
                            </div>
                            <div class="col-md-4">
                                <div class="footer_createdby">Exclusively Designed By Marque Innovation</div>
                            </div>
                            <div class="col-md-4">
                                <div class="footer_social-links">
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-pinterest"></i></a>
                                    <a href=""><i class="fa fa-youtube"></i></a>
                                    <a href=""><i class="fa fa-rss"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end: .bottom -->
            
        </div>
        <!-- end: .inner -->
    </div>
    <!-- end: .wrapper -->
</div>
<!-- end: .root -->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="assets/js/homepage.min.js"></script>
    
</body>
</html>