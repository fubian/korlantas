<!DOCTYPE html>
<html lang="en">
<head>
    <title>Website Resmi Korlantas</title>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="Website Resmi Korlantas">
    <meta name="keywords" content="korlantas, police, law">
    
    <link href="assets/css/education.min.css" rel="stylesheet">
    <script type="text/javascript" src="jquery.js"></script>
		<script>
			$(document).ready(function(){
				$("#template-header").load("template-header.html");
				$("#template-slider").load("template-slider.html");
				$("#template-sidebar").load("template-sidebar.html");
				$("#template-footerin").load("template-footer.htmlin");
				$("#template-footer").load("template-footer.html");
			});
		</script>
</head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- start: .root -->
<div class="root">
    <!-- start: .wrapper -->
    <div class="wrapper">
        <!-- start: .inner -->
        <div class="inner">
            <div id="template-header"></div>
            
            
            <!-- start: main -->
            <main>
                <div class="content_main">
                    <div class="container">
                        <div class="row">
                            <!-- start: .primary -->
                            <div class="primary col-md-8">
                            
                                <!-- start: .b_tab -->
                                <div class="b_tab">
                                    <div class="b_tab_menu clear">
                                        <a href="#edukasi-syarat">Persyaratan SIM/STNK</a>
                                        <a href="#edukasi-soal">Soal Ujian SIM</a>
                                        <a href="#edukasi-simulasi">Simulasi Ujian SIM</a>
                                    </div>
                                    <!-- start: .b_tab_content -->
                                    <div class="b_tab_content">
                                        <!-- start: .b_tab_content_item -->
                                        <div id="edukasi-syarat" class="b_tab_content_item">
                                            <div class="b_post">
                                                <!-- start: .b_post_headline -->
                                                <div class="b_post_headline">
                                                        <div class="b_post_headline_thumb">
                                                            <img src="assets/img/post-headline-1.jpg" alt="">
                                                        </div>
                                                        <div class="b_post_headline_main">
                                                            <h3 class="b_post_headline_title">
                                                                <a href="">Persyaratan Pengambilan SIM/STNK A</a>
                                                            </h3>
                                                            <div class="b_post_headline_content">
                                                                Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                                                            </div>
                                                            <div class="b_post_headline_connect clear">
                                                                <strong>Connect with Persyaratan SIM/STNK:</strong>
                                                                <div class="b_connect-link">
                                                                    <a href="#" class="b_connect-link_facebook">
                                                                        <i class="fa fa-facebook"></i>
                                                                    </a>
                                                                    <a href="#" class="b_connect-link_twitter">
                                                                        <i class="fa fa-twitter"></i>
                                                                    </a>
                                                                    <a href="#" class="b_connect-link_google-plus">
                                                                        <i class="fa fa-google-plus"></i>
                                                                    </a>
                                                                    <a href="#" class="b_connect-link_linkedin">
                                                                        <i class="fa fa-linkedin"></i>
                                                                    </a>
                                                                    <a href="#" class="b_connect-link_youtube">
                                                                        <i class="fa fa-youtube"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                <!-- end: .b_post_headline -->
                                                
                                                <!-- start: .b_post_more-link -->
                                                <div class="b_post_more-link">
                                                    <strong>
                                                        <a href="">Lokasi Layanan SIM Keliling Daerah Lain</a> <span>(2)</span>
                                                    </strong>
                                                </div>
                                                <!-- end: .b_post_more-link -->
                                                
                                                <!-- start: .b_post_list -->
                                                <div class="b_post_list">
                                                    <div class="b_post_item">
                                                        <div class="b_post_thumb">
                                                            <img src="assets/img/post-1.jpg" alt="">
                                                        </div>
                                                        <div class="b_post_main">
                                                            <div class="b_post_header">
                                                                <div class="b_post_category">Edukasi</div>
                                                                <h3 class="b_post_title">
                                                                    <a href="">Persyaratan Pembuatan SIM Baru</a>
                                                                </h3>
                                                                <div class="b_post_date">Posted on October 24, 2014</div>
                                                                <div class="b_post_comment-count">
                                                                    13 <i class="fa fa-comments"></i>
                                                                </div>
                                                            </div>
                                                            <div class="b_post_content">
                                                                Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="b_post_item">
                                                        <div class="b_post_thumb">
                                                            <img src="assets/img/post-2.jpg" alt="">
                                                        </div>
                                                        <div class="b_post_main">
                                                            <div class="b_post_header">
                                                                <div class="b_post_category">Edukasi</div>
                                                                <h3 class="b_post_title">
                                                                    <a href="">Persyaratan Perpanjangan SIM A</a>
                                                                </h3>
                                                                <div class="b_post_date">Posted on October 22, 2014</div>
                                                                <div class="b_post_comment-count">
                                                                    13 <i class="fa fa-comments"></i>
                                                                </div>
                                                            </div>
                                                            <div class="b_post_content">
                                                                Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="b_post_item">
                                                        <div class="b_post_thumb">
                                                            <img src="assets/img/post-3.jpg" alt="">
                                                        </div>
                                                        <div class="b_post_main">
                                                            <div class="b_post_header">
                                                                <div class="b_post_category">Edukasi</div>
                                                                <h3 class="b_post_title">
                                                                    <a href="">Peningkatan Golongan SIM</a>
                                                                </h3>
                                                                <div class="b_post_date">Posted on October 21, 2014</div>
                                                                <div class="b_post_comment-count">
                                                                    13 <i class="fa fa-comments"></i>
                                                                </div>
                                                            </div>
                                                            <div class="b_post_content">
                                                                Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end: .b_post_list -->
                                            </div>
                                        </div>
                                        <!-- end: .b_tab_content_item -->
                                        
                                        <!-- start: .b_tab_content_item -->
                                        <div id="edukasi-soal" class="b_tab_content_item">
                                            <div class="b_post">
                                                <!-- start: .b_post_headline -->
                                                <div class="b_post_headline">
                                                        <div class="b_post_headline_thumb">
                                                            <img src="assets/img/post-headline-1.jpg" alt="">
                                                        </div>
                                                        <div class="b_post_headline_main">
                                                            <h3 class="b_post_headline_title">
                                                                <a href="">Persyaratan Pengambilan SIM/STNK B</a>
                                                            </h3>
                                                            <div class="b_post_headline_content">
                                                                Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                                                            </div>
                                                            <div class="b_post_headline_connect clear">
                                                                <strong>Connect with Persyaratan SIM/STNK:</strong>
                                                                <div class="b_connect-link">
                                                                    <a href="#" class="b_connect-link_facebook">
                                                                        <i class="fa fa-facebook"></i>
                                                                    </a>
                                                                    <a href="#" class="b_connect-link_twitter">
                                                                        <i class="fa fa-twitter"></i>
                                                                    </a>
                                                                    <a href="#" class="b_connect-link_google-plus">
                                                                        <i class="fa fa-google-plus"></i>
                                                                    </a>
                                                                    <a href="#" class="b_connect-link_linkedin">
                                                                        <i class="fa fa-linkedin"></i>
                                                                    </a>
                                                                    <a href="#" class="b_connect-link_youtube">
                                                                        <i class="fa fa-youtube"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                <!-- end: .b_post_headline -->
                                                
                                                <!-- start: .b_post_more-link -->
                                                <div class="b_post_more-link">
                                                    <strong>
                                                        <a href="">Lokasi Layanan SIM Keliling Daerah Lain</a> <span>(2)</span>
                                                    </strong>
                                                </div>
                                                <!-- end: .b_post_more-link -->
                                                
                                                <!-- start: .b_post_list -->
                                                <div class="b_post_list">
                                                    <div class="b_post_item">
                                                        <div class="b_post_thumb">
                                                            <img src="assets/img/post-3.jpg" alt="">
                                                        </div>
                                                        <div class="b_post_main">
                                                            <div class="b_post_header">
                                                                <div class="b_post_category">Edukasi</div>
                                                                <h3 class="b_post_title">
                                                                    <a href="">Peningkatan Golongan SIM</a>
                                                                </h3>
                                                                <div class="b_post_date">Posted on October 24, 2014</div>
                                                                <div class="b_post_comment-count">
                                                                    13 <i class="fa fa-comments"></i>
                                                                </div>
                                                            </div>
                                                            <div class="b_post_content">
                                                                Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="b_post_item">
                                                        <div class="b_post_thumb">
                                                            <img src="assets/img/post-2.jpg" alt="">
                                                        </div>
                                                        <div class="b_post_main">
                                                            <div class="b_post_header">
                                                                <div class="b_post_category">Edukasi</div>
                                                                <h3 class="b_post_title">
                                                                    <a href="">Persyaratan Perpanjangan SIM A</a>
                                                                </h3>
                                                                <div class="b_post_date">Posted on October 22, 2014</div>
                                                                <div class="b_post_comment-count">
                                                                    13 <i class="fa fa-comments"></i>
                                                                </div>
                                                            </div>
                                                            <div class="b_post_content">
                                                                Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="b_post_item">
                                                        <div class="b_post_thumb">
                                                            <img src="assets/img/post-1.jpg" alt="">
                                                        </div>
                                                        <div class="b_post_main">
                                                            <div class="b_post_header">
                                                                <div class="b_post_category">Edukasi</div>
                                                                <h3 class="b_post_title">
                                                                    <a href="">Persyaratan Pembuatan SIM Baru</a>
                                                                </h3>
                                                                <div class="b_post_date">Posted on October 21, 2014</div>
                                                                <div class="b_post_comment-count">
                                                                    13 <i class="fa fa-comments"></i>
                                                                </div>
                                                            </div>
                                                            <div class="b_post_content">
                                                                Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end: .b_post_list -->
                                            </div>
                                        </div>
                                        <!-- end: .b_tab_content_item -->
                                        
                                        <!-- start: .b_tab_content_item -->
                                        <div id="edukasi-simulasi" class="b_tab_content_item">
                                            <div class="b_post">
                                                <!-- start: .b_post_headline -->
                                                <div class="b_post_headline">
                                                        <div class="b_post_headline_thumb">
                                                            <img src="assets/img/post-headline-1.jpg" alt="">
                                                        </div>
                                                        <div class="b_post_headline_main">
                                                            <h3 class="b_post_headline_title">
                                                                <a href="">Persyaratan Pengambilan SIM/STNK C</a>
                                                            </h3>
                                                            <div class="b_post_headline_content">
                                                                Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                                                            </div>
                                                            <div class="b_post_headline_connect clear">
                                                                <strong>Connect with Persyaratan SIM/STNK:</strong>
                                                                <div class="b_connect-link">
                                                                    <a href="#" class="b_connect-link_facebook">
                                                                        <i class="fa fa-facebook"></i>
                                                                    </a>
                                                                    <a href="#" class="b_connect-link_twitter">
                                                                        <i class="fa fa-twitter"></i>
                                                                    </a>
                                                                    <a href="#" class="b_connect-link_google-plus">
                                                                        <i class="fa fa-google-plus"></i>
                                                                    </a>
                                                                    <a href="#" class="b_connect-link_linkedin">
                                                                        <i class="fa fa-linkedin"></i>
                                                                    </a>
                                                                    <a href="#" class="b_connect-link_youtube">
                                                                        <i class="fa fa-youtube"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                <!-- end: .b_post_headline -->
                                                
                                                <!-- start: .b_post_more-link -->
                                                <div class="b_post_more-link">
                                                    <strong>
                                                        <a href="">Lokasi Layanan SIM Keliling Daerah Lain</a> <span>(2)</span>
                                                    </strong>
                                                </div>
                                                <!-- end: .b_post_more-link -->
                                                
                                                <!-- start: .b_post_list -->
                                                <div class="b_post_list">
                                                    <div class="b_post_item">
                                                        <div class="b_post_thumb">
                                                            <img src="assets/img/post-2.jpg" alt="">
                                                        </div>
                                                        <div class="b_post_main">
                                                            <div class="b_post_header">
                                                                <div class="b_post_category">Edukasi</div>
                                                                <h3 class="b_post_title">
                                                                    <a href="">Peningkatan Golongan SIM</a>
                                                                </h3>
                                                                <div class="b_post_date">Posted on October 24, 2014</div>
                                                                <div class="b_post_comment-count">
                                                                    13 <i class="fa fa-comments"></i>
                                                                </div>
                                                            </div>
                                                            <div class="b_post_content">
                                                                Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="b_post_item">
                                                        <div class="b_post_thumb">
                                                            <img src="assets/img/post-1.jpg" alt="">
                                                        </div>
                                                        <div class="b_post_main">
                                                            <div class="b_post_header">
                                                                <div class="b_post_category">Edukasi</div>
                                                                <h3 class="b_post_title">
                                                                    <a href="">Persyaratan Pembuatan SIM Baru</a>
                                                                </h3>
                                                                <div class="b_post_date">Posted on October 22, 2014</div>
                                                                <div class="b_post_comment-count">
                                                                    13 <i class="fa fa-comments"></i>
                                                                </div>
                                                            </div>
                                                            <div class="b_post_content">
                                                                Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="b_post_item">
                                                        <div class="b_post_thumb">
                                                            <img src="assets/img/post-3.jpg" alt="">
                                                        </div>
                                                        <div class="b_post_main">
                                                            <div class="b_post_header">
                                                                <div class="b_post_category">Edukasi</div>
                                                                <h3 class="b_post_title">
                                                                    <a href="">Persyaratan Perpanjangan SIM A</a>
                                                                </h3>
                                                                <div class="b_post_date">Posted on October 21, 2014</div>
                                                                <div class="b_post_comment-count">
                                                                    13 <i class="fa fa-comments"></i>
                                                                </div>
                                                            </div>
                                                            <div class="b_post_content">
                                                                Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end: .b_post_list -->
                                            </div>
                                        </div>
                                        <!-- end: .b_tab_content_item -->
                                        
                                        
                                    </div>
                                    <!-- end: .b_tab_content -->
                                </div>
                                <!-- end: .b_tab -->
                            
                            </div>
                            <!-- end: .primary -->
                            
                            <!-- start: .sidebar -->
                            <div id="template-sidebar"></div>
                            <!-- end: .sidebar -->
                            
                        </div>
                    </div>
                </div>
            </main>
            <!-- end: main -->
            
            <!-- start: .bottom -->
            <div class="bottom">
                <div class="bottom_content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 section">
                                <div class="section_inner bottom_about-us">
                                    <h2 class="section_title">About Us</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                                </div>
                            </div>
                            <div class="col-md-4 section">
                                <div class="section_inner bottom_news">
                                    <h2 class="section_title">Latest News</h2>
                                    <div class="bottom_news_list">
                                        <div class="bottom_news_item clear">
                                            <div class="bottom_news_item_thumb">
                                                <img src="assets/img/news-1.jpg" alt="">
                                            </div>
                                            <div class="bottom_news_item_content">
                                                <div class="bottom_news_item_category">
                                                    Sosialita
                                                </div>
                                                <h3 class="bottom_news_item_title">Bentrok di Timika, 100 Orang Tewas</h3>
                                            </div>
                                        </div>
                                        <div class="bottom_news_item clear">
                                            <div class="bottom_news_item_thumb">
                                                <img src="assets/img/news-2.jpg" alt="">
                                            </div>
                                            <div class="bottom_news_item_content">
                                                <div class="bottom_news_item_category">
                                                    Hiburan
                                                </div>
                                                <h3 class="bottom_news_item_title">Kuartet Pembawa Berita Lalu Lintas Beraksi</h3>
                                            </div>
                                        </div>
                                        <div class="bottom_news_item clear">
                                            <div class="bottom_news_item_thumb">
                                                <img src="assets/img/news-3.jpg" alt="">
                                            </div>
                                            <div class="bottom_news_item_content">
                                                <div class="bottom_news_item_category">
                                                    Teknologi
                                                </div>
                                                <h3 class="bottom_news_item_title">Polri Dilengkapi Peralatan dan Gear Baru</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 section">
                                <div class="section_inner">
                                    <h2 class="section_title">News Line Navigation</h2>
                                    <nav class="bottom_nav">
                                        <ul>
                                            <li><i class="fa fa-caret-right"></i><a href="">Home</a></li>
                                            <li><i class="fa fa-caret-right"></i><a href="">About Us</a></li>
                                            <li><i class="fa fa-caret-right"></i><a href="">CCTV</a></li>
                                            <li><i class="fa fa-caret-right"></i><a href="">Info</a></li>
                                            <li><i class="fa fa-caret-right"></i><a href="">Edukasi</a></li>
                                            <li><i class="fa fa-caret-right"></i><a href="">Contact Us</a></li>
                                            <li><i class="fa fa-caret-right"></i><a href="">Layanan Masyarakat</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="footer_copyright"><small>Copyright &copy; NTMC Polri</small></div>
                            </div>
                            <div class="col-md-4">
                                <div class="footer_createdby">Exclusively Designed By Marque Innovation</div>
                            </div>
                            <div class="col-md-4">
                                <div class="footer_social-links">
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-pinterest"></i></a>
                                    <a href=""><i class="fa fa-youtube"></i></a>
                                    <a href=""><i class="fa fa-rss"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end: .bottom -->
            
        </div>
        <!-- end: .inner -->
    </div>
    <!-- end: .wrapper -->
</div>
<!-- end: .root -->

<div class="b_page_overlay"></div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="assets/js/education.min.js"></script>
    
</body>
</html>